import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewEditMachineryComponent } from './new-edit-machinery.component';

describe('NewEditMachineryComponent', () => {
  let component: NewEditMachineryComponent;
  let fixture: ComponentFixture<NewEditMachineryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewEditMachineryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEditMachineryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
