import { Component, OnInit } from '@angular/core';
import {TypeDocumentService} from '../../type-document/shared/services/type-document.service';
import {ProviderService} from '../../provider/shared/services/provider.service';
import {Provider} from '../../provider/shared/models/provider.model';
import {TypeDocument} from '../../type-document/shared/models/type-document.model';
import {ActivatedRoute, Router} from '@angular/router';
import {Machinery} from '../shared/models/machinery.model';
import {State} from '../../states/shared/models/state.model';
import {StateService} from '../../states/shared/services/state.service';
import {MachineryService} from '../shared/services/machinery.service';

@Component({
  selector: 'app-new-edit-machinery',
  templateUrl: './new-edit-machinery.component.html',
  styleUrls: ['./new-edit-machinery.component.css']
})
export class NewEditMachineryComponent implements OnInit {

  machinery = new Machinery();
  states: State[] = [];
  providers: Provider[] = [];

  constructor(private providerService: ProviderService,
              private stateService: StateService,
              private machineryService: MachineryService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if (params.get('id')) {
        this.machinery.id = parseInt(params.get('id'), 10);
        this.initMachinery();
      }
    });

    this.initStates();
    this.initProviders();
  }

  initMachinery() {
    this.machineryService.getMachineryById(this.machinery.id).subscribe(
      response => {
        console.log('machinery:', response);
        this.machinery.descripcion = response.descripcion;
        this.machinery.fechaActivacion = response.fechaActivacion;
        this.machinery.fechaBaja = response.fechaBaja;
        this.machinery.valorAdquisicion = response.valorAdquisicion;
        this.machinery.idProveedor = response.idProveedor;
        this.machinery.nombreProveedor = response.nombreProveedor;
        this.machinery.idEstado = response.idEstado;
        this.machinery.nombreEstado = response.nombreEstado;
      }
    );
  }

  initStates() {
    this.stateService.getStates().subscribe(
      response => {
        this.states = response;
      }
    );
  }

  initProviders(): void {
    this.providerService.getProviders().subscribe(
      response => {
        console.log(response);
        this.providers = response;
      },
      error => {
        console.log(error);
      }
    );
  }

  saveMachinery(): void {
    if (this.machinery.id > 0) {
      this.machineryService.updateMachinery(this.machinery).subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/machineries']);
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.machineryService.saveMachinery(this.machinery).subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/machineries']);
        },
        error => {
          console.log(error);
        }
      );
    }
  }

}
