import { Component, OnInit } from '@angular/core';
import {MachineryService} from '../shared/services/machinery.service';
import {MatTableDataSource} from '@angular/material';


@Component({
  selector: 'app-machineries',
  templateUrl: './machineries.component.html',
  styleUrls: ['./machineries.component.css']
})
export class MachineriesComponent implements OnInit {

  machineryColumns = ['id', 'descripcion', 'fechaActivacion', 'fechaBaja', 'nombreProveedor', 'nombreEstado', 'acciones'];
  dataMachinery = null;
  constructor(private machineryService: MachineryService) { }

  ngOnInit() {
    this.initMachinery();
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataMachinery.filter = filterValue;
  }
  initMachinery(): void {
    this.machineryService.getMachineries().subscribe(
      response => {
        console.log(response);
        this.dataMachinery = new MatTableDataSource(response);
      },
      error => {
        console.log(error);
      }
    );
  }
  onDeleteMachinery(id) {
    this.machineryService.deleteMachinery(id).subscribe(
      response => {
        console.log(response);
        this.initMachinery();
      },
      error => {
        console.log(error);
      }
    );
  }
}
