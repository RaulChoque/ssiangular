import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MachineriesComponent } from './machineries/machineries.component';
import { NewEditMachineryComponent } from './new-edit-machinery/new-edit-machinery.component';
import {CustomMaterialModule} from '../custom-material/custom-material.module';
import {FormsModule} from '@angular/forms';
import {MachineryRoutingModule} from './machinery-routing.module';
import {MachineryService} from './shared/services/machinery.service';

@NgModule({
  imports: [
    CommonModule,
    CustomMaterialModule,
    FormsModule,
    MachineryRoutingModule
  ],
  declarations: [
    MachineriesComponent,
    NewEditMachineryComponent
  ],
  providers: [
    MachineryService
  ]
})
export class MachineryModule { }
