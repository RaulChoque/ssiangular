import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {MachineriesComponent} from './machineries/machineries.component';
import {NewEditMachineryComponent} from './new-edit-machinery/new-edit-machinery.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {path: 'machineries', component: MachineriesComponent},
      {path: 'machinery/new', component: NewEditMachineryComponent},
      {path: 'machinery/:id/edit', component: NewEditMachineryComponent}
    ])
  ],
  exports: [RouterModule]
})
export class MachineryRoutingModule {
}
