import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CONFIG} from '../../../shared/util/global';
import {Observable} from 'rxjs/Observable';
import {Machinery} from '../models/machinery.model';

@Injectable()
export class MachineryService {

  private url = CONFIG.url + '/maquinarias';

  constructor(private http: HttpClient) {
  }

  getMachineries(): Observable<any> {
    return this.http.get(this.url);
  }

  getMachineryById(id: number): Observable<any> {
    return this.http.get(this.url + '/' + id);
  }

  saveMachinery(machinery: Machinery): Observable<any> {
    return this.http.post(this.url, machinery);
  }

  updateMachinery(machinery: Machinery): Observable<any> {
    return this.http.put(this.url, machinery);
  }

  deleteMachinery(id: number): Observable<any> {
    return this.http.delete(this.url + '/' + id);
  }

}
