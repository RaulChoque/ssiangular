export class Machinery {
  id: number;
  descripcion: string;
  fechaActivacion: string;
  fechaBaja: string;
  valorAdquisicion: number;
  idProveedor: number;
  nombreProveedor: string;
  idEstado: number;
  nombreEstado: string;

  constructor(id?: number, descripcion?: string, fechaActivacion?: string, fechaBaja?: string, valorAdquisicion?: number, idProveedor?: number, nombreProveedor?: string, idEstado?: number, nombreEstado?: string  ) {
    this.id = id || 0;
    this.descripcion = descripcion || null;
    this.fechaActivacion = fechaActivacion || null;
    this.fechaBaja = fechaBaja || null;
    this.valorAdquisicion = valorAdquisicion || 0;
    this.idProveedor = idProveedor || 0;
    this.nombreProveedor = nombreProveedor || null;
    this.idEstado = idEstado || 0;
    this.nombreEstado = nombreEstado || null;

  }
}


