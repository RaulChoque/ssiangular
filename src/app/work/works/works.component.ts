import { Component, OnInit } from '@angular/core';
import {WorkService} from '../shared/services/work.service';
import {Work} from '../shared/models/work.model';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-works',
  templateUrl: './works.component.html',
  styleUrls: ['./works.component.css']
})
export class WorksComponent implements OnInit {

  workColumns = ['id', 'nombre', 'direccion', 'ciudad', 'presupuesto', 'acciones'];
  dataWorks = null;

  constructor(private workService: WorkService) { }

  ngOnInit() {
    this.initWorks();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataWorks.filter = filterValue;
  }

  initWorks() {
    this.workService.getWorks().subscribe(
      response => {
        this.dataWorks = new MatTableDataSource(response);
      }
    );
  }

}
