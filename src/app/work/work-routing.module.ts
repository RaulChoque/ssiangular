import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {WorksComponent} from './works/works.component';
import {ViewWorkComponent} from './view-work/view-work.component';
import {NewEditWorkComponent} from './new-edit-work/new-edit-work.component';


@NgModule({
  imports: [
    RouterModule.forChild([
      {path: 'works', component: WorksComponent},
      {path: 'work/new', component: NewEditWorkComponent},
      {path: 'work/:id/edit', component: NewEditWorkComponent},
      {path: 'work/:id/view', component: ViewWorkComponent}
    ])
  ],
  exports: [RouterModule]
})
export class WorkRoutingModule {
}
