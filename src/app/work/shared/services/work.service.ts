import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {CONFIG} from '../../../shared/util/global';
import {HttpClient} from '@angular/common/http';
import {Work} from '../models/work.model';

@Injectable()
export class WorkService {

  private url = CONFIG.url + '/obras';

  constructor(private http: HttpClient) {
  }

  getWorks(): Observable<any> {
    return this.http.get(this.url);
  }

  getWorkById(id: number): Observable<any> {
    return this.http.get(this.url + '/' + id);
  }

  saveWork(work: Work): Observable<any> {
    return this.http.post(this.url, work);
  }

  updateWork(work: Work): Observable<any> {
    return this.http.put(this.url, work);
  }

  deleteWork(id: number): Observable<any> {
    return this.http.delete(this.url + '/' + id);
  }
}
