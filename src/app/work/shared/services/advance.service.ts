import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {CONFIG} from '../../../shared/util/global';
import {HttpClient} from '@angular/common/http';
import {State} from '../../../states/shared/models/state.model';
import {Advance} from '../models/advance.model';

@Injectable()
export class AdvanceService {

  private url = CONFIG.url + '/avances';

  constructor(private http: HttpClient) {
  }

  getAdvances(): Observable<any> {
    return this.http.get(this.url);
  }

  getAdvanceById(id: number): Observable<any> {
    return this.http.get(this.url + '/' + id);
  }

  saveAdvance(advance: Advance): Observable<any> {
    return this.http.post(this.url, advance);
  }

  updateAdvance(advance: Advance): Observable<any> {
    return this.http.put(this.url, advance);
  }

  deleteAdvance(id: number): Observable<any> {
    return this.http.delete(this.url + '/' + id);
  }

  getAdvancesByWorkId(workId: number): Observable<any> {
    return this.http.get(this.url + '/obra/' + workId);
  }

}
