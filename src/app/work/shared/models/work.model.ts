export class Work {
  id: number;
  nombre: string;
  direccion: string;
  ciudad: string;
  presupuesto: number;
  idEstadoObra: number;

  constructor(id?: number, nombre?: string, direccion?: string, ciudad?: string, presupuesto?: number, idEstadoObra?: number) {
    this.id = id || 0;
    this.nombre = nombre || null;
    this.direccion = direccion || null;
    this.ciudad = ciudad || null;
    this.presupuesto = presupuesto || null;
    this.idEstadoObra = idEstadoObra || null;
  }
}
