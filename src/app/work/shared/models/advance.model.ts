export class Advance {
  id: number;
  descripcion: string;
  fecha: any;
  costo: number;
  idObra: number;

  constructor(id?: number, descripcion?: string, fecha?: any, costo?: number, idObra?: number) {
    this.id = id || 0;
    this.descripcion = descripcion || null;
    this.fecha = fecha || null;
    this.costo = costo || 0;
    this.idObra = idObra || 0;
  }
}
