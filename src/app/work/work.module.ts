import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorksComponent } from './works/works.component';
import {CustomMaterialModule} from '../custom-material/custom-material.module';
import {WorkRoutingModule} from './work-routing.module';
import {WorkService} from './shared/services/work.service';
import { ViewWorkComponent } from './view-work/view-work.component';
import {AdvanceService} from './shared/services/advance.service';
import { NewEditWorkComponent } from './new-edit-work/new-edit-work.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CustomMaterialModule,
    WorkRoutingModule
  ],
  declarations: [
    WorksComponent,
    ViewWorkComponent,
    NewEditWorkComponent
  ],
  providers: [
    WorkService,
    AdvanceService
  ]
})
export class WorkModule { }
