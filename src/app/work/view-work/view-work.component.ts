import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Work} from '../shared/models/work.model';
import {WorkService} from '../shared/services/work.service';
import {AdvanceService} from '../shared/services/advance.service';

@Component({
  selector: 'app-view-work',
  templateUrl: './view-work.component.html',
  styleUrls: ['./view-work.component.css']
})
export class ViewWorkComponent implements OnInit {

  work: Work = null;
  advanceColumns = ['id', 'fecha', 'descripcion', 'costo'];
  dataAdvances = null;

  constructor(private route: ActivatedRoute,
              private workService: WorkService,
              private advanceService: AdvanceService
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if (params.get('id')) {
        this.work = new Work();
        this.work.id = parseInt(params.get('id'), 10);
        this.initWork();
        this.initAdvances();
      }
    });
  }

  initWork() {
    this.workService.getWorkById(this.work.id).subscribe(
      response => {
        this.work.nombre = response.nombre;
        this.work.ciudad = response.ciudad;
        this.work.direccion = response.direccion;
        this.work.presupuesto = response.presupuesto;
      }
    );
  }

  initAdvances() {
    this.advanceService.getAdvancesByWorkId(this.work.id).subscribe(
      response => {
        this.dataAdvances = response;
      }
    );
  }

}
