import { Component, OnInit } from '@angular/core';
import {Work} from '../shared/models/work.model';
import {WorkService} from '../shared/services/work.service';
import {ActivatedRoute, Router} from '@angular/router';
import {StateService} from '../../states/shared/services/state.service';
import {State} from '../../states/shared/models/state.model';

@Component({
  selector: 'app-new-edit-work',
  templateUrl: './new-edit-work.component.html',
  styleUrls: ['./new-edit-work.component.css']
})
export class NewEditWorkComponent implements OnInit {

  work = new Work();
  states: State[] = [];

  constructor(private workService: WorkService,
              private stateService: StateService,
              private router: Router,
              private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe( params => {
      if ( params.get('id') ) {
        this.work.id = parseInt(params.get('id'), 10);
        this.initWork();
      }
    });
    this.initStates();
  }

  initWork() {
    this.workService.getWorkById(this.work.id).subscribe(
      response => {
        this.work.nombre = response.nombre;
        this.work.direccion = response.direccion;
        this.work.ciudad = response.ciudad;
        this.work.presupuesto = response.presupuesto;
        this.work.idEstadoObra = response.idEstadoObra;
      }
    );
  }

  initStates(): void {
    this.stateService.getStates().subscribe(
      response => {
        console.log(response);
        this.states = this.stateService.getStatesByType(response, 'obra');
      },
      error => {
        console.log(error);
      }
    );
  }

  onSelectionChange(event) {

  }

  saveWork(): void {
    if (this.work.id > 0) {
      this.workService.updateWork(this.work).subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/works']);
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.workService.saveWork(this.work).subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/works']);
        },
        error => {
          console.log(error);
        }
      );
    }
  }
}
