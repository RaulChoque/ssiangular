import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewEditWorkComponent } from './new-edit-work.component';

describe('NewEditWorkComponent', () => {
  let component: NewEditWorkComponent;
  let fixture: ComponentFixture<NewEditWorkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewEditWorkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEditWorkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
