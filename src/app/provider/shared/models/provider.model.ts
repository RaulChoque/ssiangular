import {TypeDocument} from '../../../type-document/shared/models/type-document.model';

export class Provider {
  id: number;
  nombreComercial: string;
  razonSocial: string;
  numeroDocumento: string;
  direccion: string;
  diasPlazo: number;
  telefono: number;
  correo: string;
  idTipoDocumento: number;
  descripcionTipoDocumento: string;

  constructor(id?: number, nombreComercial?: string, razonSocial?: string, numeroDocumento?: string, direccion?: string, diasPlazo?: number,
              telefono?: number, correo?: string, idTipoDocumento?: number, descripcionTipoDocumento?: string) {
    this.id = id || 0;
    this.nombreComercial = nombreComercial || null;
    this.razonSocial = razonSocial || null;
    this.numeroDocumento = numeroDocumento || null;
    this.direccion = direccion || null;
    this.diasPlazo = diasPlazo || null;
    this.telefono = telefono || null;
    this.correo = correo || null;
    this.idTipoDocumento = idTipoDocumento || null;
    this.descripcionTipoDocumento = descripcionTipoDocumento || null;
  }
}
