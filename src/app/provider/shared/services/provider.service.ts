import { Injectable } from '@angular/core';
import {CONFIG} from '../../../shared/util/global';
import {State} from '../../../states/shared/models/state.model';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {Provider} from '../models/provider.model';

@Injectable()
export class ProviderService {

  private url = CONFIG.url + '/proveedores';

  constructor(private http: HttpClient) {
  }

  getProviders(): Observable<any> {
    return this.http.get(this.url);
  }

  getProviderById(id: number): Observable<any> {
    return this.http.get(this.url + '/' + id);
  }

  saveProvider(provider: Provider): Observable<any> {
    return this.http.post(this.url, provider);
  }

  updateProvider(provider: Provider): Observable<any> {
    return this.http.put(this.url, provider);
  }

}
