import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {ProvidersComponent} from './providers/providers.component';
import {NewEditProviderComponent} from './new-edit-provider/new-edit-provider.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {path: 'providers', component: ProvidersComponent},
      {path: 'provider/new', component: NewEditProviderComponent},
      {path: 'provider/:id/edit', component: NewEditProviderComponent}
    ])
  ],
  exports: [RouterModule]
})
export class ProviderRoutingModule {
}
