import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewEditProviderComponent } from './new-edit-provider.component';

describe('NewEditProviderComponent', () => {
  let component: NewEditProviderComponent;
  let fixture: ComponentFixture<NewEditProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewEditProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEditProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
