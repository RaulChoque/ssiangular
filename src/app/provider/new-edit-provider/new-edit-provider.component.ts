import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Provider} from '../shared/models/provider.model';
import {ProviderService} from '../shared/services/provider.service';
import {MatTableDataSource} from '@angular/material';
import {TypeDocumentService} from '../../type-document/shared/services/type-document.service';
import {TypeDocument} from '../../type-document/shared/models/type-document.model';

@Component({
  selector: 'app-new-edit-provider',
  templateUrl: './new-edit-provider.component.html',
  styleUrls: ['./new-edit-provider.component.css']
})
export class NewEditProviderComponent implements OnInit {

  provider = new Provider();
  typeDocuments: TypeDocument[] = [];
  typeDocument: TypeDocument = new TypeDocument();

  constructor(private providerService: ProviderService,
              private typeDocumentService: TypeDocumentService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if (params.get('id')) {
        this.provider.id = parseInt(params.get('id'), 10);
        this.initProvider();
      }
    });

    this.initTypeDocuments();
  }

  initProvider() {
    this.providerService.getProviderById(this.provider.id).subscribe(
      response => {
        console.log('provider:', response);
        this.provider.nombreComercial = response.nombreComercial;
        this.provider.razonSocial = response.razonSocial;
        this.provider.numeroDocumento = response.numeroDocumento;
        this.provider.direccion = response.direccion;
        this.provider.diasPlazo = response.diasPlazo;
        this.provider.telefono = response.telefono;
        this.provider.correo = response.correo;
        this.provider.idTipoDocumento = response.idTipoDocumento;
        this.provider.descripcionTipoDocumento = response.descripcionTipoDocumento;

        this.initTypeDocument(response.idTipoDocumento);
      }
    );
  }

  initTypeDocument(id: number) {
    this.typeDocumentService.getTypeDocumentById(id).subscribe(
      response => {
        this.typeDocument.id = response.id;
        this.typeDocument.descripcion = response.descripcion;
        this.typeDocument.observaciones = response.observaciones;
      }
    );
  }

  initTypeDocuments(): void {
    this.typeDocumentService.getTypeDocuments().subscribe(
      response => {
        console.log(response);
        this.typeDocuments = response;
      },
      error => {
        console.log(error);
      }
    );
  }

  onSelectionChange(event) {
    if (event.value != null) {
      this.provider.idTipoDocumento = event.value;
      // this.provider.descripcionTipoDocumento = event.value.descripcion;
    } else {
      this.provider.idTipoDocumento = 0;
      // this.provider.descripcionTipoDocumento = null;
    }
  }

  saveProvider(): void {
    if (this.provider.id > 0) {
      this.providerService.updateProvider(this.provider).subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/providers']);
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.providerService.saveProvider(this.provider).subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/providers']);
        },
        error => {
          console.log(error);
        }
      );
    }
  }

}
