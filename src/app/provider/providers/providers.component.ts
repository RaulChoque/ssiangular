import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {StateService} from '../../states/shared/services/state.service';
import {ProviderService} from '../shared/services/provider.service';

@Component({
  selector: 'app-providers',
  templateUrl: './providers.component.html',
  styleUrls: ['./providers.component.css']
})
export class ProvidersComponent implements OnInit {

  providerColumns = ['id', 'nombreComercial', 'razonSocial', 'numeroDocumento', 'direccion', 'diasPlazo', 'telefono', 'correo', 'descripcionTipoDocumento', 'acciones'];
  dataProviders = null;

  constructor(private providerService: ProviderService) { }

  ngOnInit() {
    this.initProviders();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataProviders.filter = filterValue;
  }

  initProviders(): void {
    this.providerService.getProviders().subscribe(
      response => {
        console.log(response);
        this.dataProviders = new MatTableDataSource(response);
      },
      error => {
        console.log(error);
      }
    );
  }

}
