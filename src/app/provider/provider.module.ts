import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProvidersComponent } from './providers/providers.component';
import { NewEditProviderComponent } from './new-edit-provider/new-edit-provider.component';
import {ProviderService} from './shared/services/provider.service';
import {FormsModule} from '@angular/forms';
import {CustomMaterialModule} from '../custom-material/custom-material.module';
import {ProviderRoutingModule} from './provider-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CustomMaterialModule,
    ProviderRoutingModule
  ],
  declarations: [
    ProvidersComponent,
    NewEditProviderComponent
  ],
  providers: [
    ProviderService
  ]
})
export class ProviderModule { }
