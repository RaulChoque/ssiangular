import {Injectable} from '@angular/core';
import {CONFIG} from '../../../shared/util/global';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {State} from '../models/state.model';

@Injectable()
export class StateService {

  private url = CONFIG.url + '/estados';

  constructor(private http: HttpClient) {
  }

  getStates(): Observable<any> {
    return this.http.get(this.url);
  }

  getStateById(id: number): Observable<any> {
    return this.http.get(this.url + '/' + id);
  }

  saveState(state: State): Observable<any> {
    return this.http.post(this.url, state);
  }

  updateState(state: State): Observable<any> {
    return this.http.put(this.url, state);
  }

  deleteState(id: number): Observable<any> {
    return this.http.delete(this.url + '/' + id);
  }

  getStatesByType(states: any, type: string) {
    const stateData = [];
    states.forEach(state => {
      const name = state.nombre;
      if (name.toUpperCase() === type.toUpperCase()) {
        stateData.push(state);
      }
    });
    return stateData;
  }

}
