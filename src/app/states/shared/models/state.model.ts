export class State {
  id: number;
  nombre: string;
  descripcion: string;

  constructor(id?: number, nombre?: string, descripcion?: string) {
    this.id = id || 0;
    this.nombre = nombre || null;
    this.descripcion = descripcion || null;
  }
}
