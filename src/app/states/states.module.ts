import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StatesComponent} from './states/states.component';
import {NewEditStateComponent} from './new-edit-state/new-edit-state.component';
import {CustomMaterialModule} from '../custom-material/custom-material.module';
import {StatesRoutingModule} from './states-routing.module';
import {StateService} from './shared/services/state.service';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CustomMaterialModule,
    StatesRoutingModule
  ],
  declarations: [
    StatesComponent,
    NewEditStateComponent
  ],
  providers: [
    StateService
  ]
})
export class StatesModule {
}
