import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {StateService} from '../shared/services/state.service';

@Component({
  selector: 'app-states',
  templateUrl: './states.component.html',
  styleUrls: ['./states.component.css']
})
export class StatesComponent implements OnInit {

  stateColumns = ['id', 'nombre', 'descripcion', 'acciones'];
  dataStates = null;

  constructor(private stateService: StateService) { }

  ngOnInit() {
    this.initStates();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataStates.filter = filterValue;
  }

  initStates(): void {
    this.stateService.getStates().subscribe(
      response => {
        console.log(response);
        this.dataStates = new MatTableDataSource(response);
      },
      error => {
        console.log(error);
      }
    );
  }

  onDeleteState(id) {
    this.stateService.deleteState(id).subscribe(
      response => {
        console.log(response);
        this.initStates();
      },
      error => {
        console.log(error);
      }
    );
  }

}
