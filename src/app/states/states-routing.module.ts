import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {StatesComponent} from './states/states.component';
import {NewEditStateComponent} from './new-edit-state/new-edit-state.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {path: 'states', component: StatesComponent},
      {path: 'state/new', component: NewEditStateComponent},
      {path: 'state/:id/edit', component: NewEditStateComponent}
    ])
  ],
  exports: [RouterModule]
})
export class StatesRoutingModule {
}
