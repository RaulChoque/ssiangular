import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewEditStateComponent } from './new-edit-state.component';

describe('NewEditStateComponent', () => {
  let component: NewEditStateComponent;
  let fixture: ComponentFixture<NewEditStateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewEditStateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEditStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
