import { Component, OnInit } from '@angular/core';
import {State} from '../shared/models/state.model';
import {StateService} from '../shared/services/state.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-new-edit-state',
  templateUrl: './new-edit-state.component.html',
  styleUrls: ['./new-edit-state.component.css']
})
export class NewEditStateComponent implements OnInit {

  state = new State();

  constructor(private stateService: StateService,
              private router: Router,
              private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if ( params.get('id') ) {
        this.state.id = parseInt(params.get('id'), 10);
        this.initState();
      }
    });
  }

  initState() {
    this.stateService.getStateById(this.state.id).subscribe(
      response => {
        this.state.nombre = response.nombre;
        this.state.descripcion = response.descripcion;
      }
    );
  }

  saveState(): void {
    if (this.state.id > 0) {
      this.stateService.updateState(this.state).subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/states']);
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.stateService.saveState(this.state).subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/states']);
        },
        error => {
          console.log(error);
        }
      );
    }
  }
}
