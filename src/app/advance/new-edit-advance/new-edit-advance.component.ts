import { Component, OnInit } from '@angular/core';
import {TypeDocumentService} from '../../type-document/shared/services/type-document.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TypeDocument} from '../../type-document/shared/models/type-document.model';
import {ProviderService} from '../../provider/shared/services/provider.service';
import {Advance} from '../../work/shared/models/advance.model';
import {Work} from '../../work/shared/models/work.model';
import {AdvanceService} from '../../work/shared/services/advance.service';
import {WorkService} from '../../work/shared/services/work.service';

@Component({
  selector: 'app-new-edit-advance',
  templateUrl: './new-edit-advance.component.html',
  styleUrls: ['./new-edit-advance.component.css']
})
export class NewEditAdvanceComponent implements OnInit {

  advance = new Advance();
  works: Work[] = [];

  constructor(private advanceService: AdvanceService,
              private workService: WorkService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if (params.get('id')) {
        this.advance.id = parseInt(params.get('id'), 10);
        this.initAdvance();
      }
    });

    this.initWorks();
  }

  initAdvance() {
    this.advanceService.getAdvanceById(this.advance.id).subscribe(
      response => {
        console.log('advance:', response);
        this.advance.descripcion = response.descripcion;
        this.advance.fecha = response.fecha;
        this.advance.costo = response.costo;
        this.advance.idObra = response.idObra;
      }
    );
  }

  initWorks(): void {
    this.workService.getWorks().subscribe(
      response => {
        console.log(response);
        this.works = response;
      },
      error => {
        console.log(error);
      }
    );
  }

  saveAdvance(): void {
    if (this.advance.id > 0) {
      this.advanceService.updateAdvance(this.advance).subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/advances']);
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.advanceService.saveAdvance(this.advance).subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/advances']);
        },
        error => {
          console.log(error);
        }
      );
    }
  }


}
