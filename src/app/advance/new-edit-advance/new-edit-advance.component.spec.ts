import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewEditAdvanceComponent } from './new-edit-advance.component';

describe('NewEditAdvanceComponent', () => {
  let component: NewEditAdvanceComponent;
  let fixture: ComponentFixture<NewEditAdvanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewEditAdvanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEditAdvanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
