import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdvancesComponent } from './advances/advances.component';
import { NewEditAdvanceComponent } from './new-edit-advance/new-edit-advance.component';
import {CustomMaterialModule} from '../custom-material/custom-material.module';
import {FormsModule} from '@angular/forms';
import {AdvanceRoutingModule} from './advance-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CustomMaterialModule,
    FormsModule,
    AdvanceRoutingModule
  ],
  declarations: [
    AdvancesComponent,
    NewEditAdvanceComponent
  ]
})
export class AdvanceModule { }
