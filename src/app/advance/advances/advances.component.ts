import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {AdvanceService} from '../../work/shared/services/advance.service';

@Component({
  selector: 'app-advances',
  templateUrl: './advances.component.html',
  styleUrls: ['./advances.component.css']
})
export class AdvancesComponent implements OnInit {

  advanceColumns = ['id', 'fecha', 'descripcion', 'costo', 'acciones'];
  dataAdvances = null;

  constructor(private advanceService: AdvanceService) { }

  ngOnInit() {
    this.initAdvances();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataAdvances.filter = filterValue;
  }

  initAdvances(): void {
    this.advanceService.getAdvances().subscribe(
      response => {
        console.log(response);
        this.dataAdvances = new MatTableDataSource(response);
      },
      error => {
        console.log(error);
      }
    );
  }
}
