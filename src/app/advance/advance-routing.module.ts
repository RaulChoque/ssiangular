import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {NewEditAdvanceComponent} from './new-edit-advance/new-edit-advance.component';
import {AdvancesComponent} from './advances/advances.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {path: 'advances', component: AdvancesComponent},
      {path: 'advance/new', component: NewEditAdvanceComponent},
      {path: 'advance/:id/edit', component: NewEditAdvanceComponent},
    ])
  ],
  exports: [RouterModule]
})
export class AdvanceRoutingModule {
}
