import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TypeDocumentsComponent } from './type-documents/type-documents.component';
import { NewEditTypeDocumentComponent } from './new-edit-type-document/new-edit-type-document.component';
import {FormsModule} from '@angular/forms';
import {CustomMaterialModule} from '../custom-material/custom-material.module';
import {TypeDocumentRoutingModule} from './type-document-routing.module';
import {TypeDocumentService} from './shared/services/type-document.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CustomMaterialModule,
    TypeDocumentRoutingModule
  ],
  declarations: [
    TypeDocumentsComponent,
    NewEditTypeDocumentComponent
  ],
  providers: [
    TypeDocumentService
  ]
})
export class TypeDocumentModule { }
