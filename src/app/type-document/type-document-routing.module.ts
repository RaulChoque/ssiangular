import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TypeDocumentsComponent} from './type-documents/type-documents.component';
import {NewEditTypeDocumentComponent} from './new-edit-type-document/new-edit-type-document.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {path: 'type-documents', component: TypeDocumentsComponent},
      {path: 'type-document/new', component: NewEditTypeDocumentComponent},
      {path: 'type-document/:id/edit', component: NewEditTypeDocumentComponent}
    ])
  ],
  exports: [RouterModule]
})
export class TypeDocumentRoutingModule {
}
