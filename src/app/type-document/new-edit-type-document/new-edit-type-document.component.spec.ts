import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewEditTypeDocumentComponent } from './new-edit-type-document.component';

describe('NewEditTypeDocumentComponent', () => {
  let component: NewEditTypeDocumentComponent;
  let fixture: ComponentFixture<NewEditTypeDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewEditTypeDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEditTypeDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
