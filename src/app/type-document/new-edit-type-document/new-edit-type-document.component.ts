import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TypeDocumentService} from '../shared/services/type-document.service';
import {TypeDocument} from '../shared/models/type-document.model';

@Component({
  selector: 'app-new-edit-type-document',
  templateUrl: './new-edit-type-document.component.html',
  styleUrls: ['./new-edit-type-document.component.css']
})
export class NewEditTypeDocumentComponent implements OnInit {

  typeDocument = new TypeDocument();

  constructor(private typeDocumentService: TypeDocumentService,
              private router: Router,
              private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if ( params.get('id') ) {
        this.typeDocument.id = parseInt(params.get('id'), 10);
        this.initTypeDocument();
      }
    });
  }

  initTypeDocument() {
    this.typeDocumentService.getTypeDocumentById(this.typeDocument.id).subscribe(
      response => {
        this.typeDocument.descripcion = response.descripcion;
        this.typeDocument.observaciones = response.observaciones;
      }
    );
  }

  saveTypeDocument(): void {
    if (this.typeDocument.id > 0) {
      this.typeDocumentService.updateTypeDocument(this.typeDocument).subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/type-documents']);
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.typeDocumentService.saveTypeDocument(this.typeDocument).subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/type-documents']);
        },
        error => {
          console.log(error);
        }
      );
    }
  }

}
