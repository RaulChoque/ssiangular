import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {StateService} from '../../states/shared/services/state.service';
import {TypeDocumentService} from '../shared/services/type-document.service';

@Component({
  selector: 'app-type-documents',
  templateUrl: './type-documents.component.html',
  styleUrls: ['./type-documents.component.css']
})
export class TypeDocumentsComponent implements OnInit {

  typeDocumentColumns = ['id', 'descripcion', 'observaciones', 'acciones'];
  dataTypeDocuments = null;

  constructor(private typeDocumentService: TypeDocumentService) { }

  ngOnInit() {
    this.initTypeDocuments();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataTypeDocuments.filter = filterValue;
  }

  initTypeDocuments(): void {
    this.typeDocumentService.getTypeDocuments().subscribe(
      response => {
        console.log(response);
        this.dataTypeDocuments = new MatTableDataSource(response);
      },
      error => {
        console.log(error);
      }
    );
  }

  onDeleteTypeDocument(id) {
    this.typeDocumentService.deleteTypeDocument(id).subscribe(
      response => {
        console.log(response);
        this.initTypeDocuments();
      },
      error => {
        console.log(error);
      }
    );
  }

}
