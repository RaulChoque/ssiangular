import { Injectable } from '@angular/core';
import {CONFIG} from '../../../shared/util/global';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {TypeDocument} from '../models/type-document.model';

@Injectable()
export class TypeDocumentService {

  private url = CONFIG.url + '/tipodocumentos';

  constructor(private http: HttpClient) {
  }

  getTypeDocuments(): Observable<any> {
    return this.http.get(this.url);
  }

  getTypeDocumentById(id: number): Observable<any> {
    return this.http.get(this.url + '/' + id);
  }

  saveTypeDocument(typeDocument: TypeDocument): Observable<any> {
    return this.http.post(this.url, typeDocument);
  }

  updateTypeDocument(typeDocument: TypeDocument): Observable<any> {
    return this.http.put(this.url, typeDocument);
  }
  deleteTypeDocument(id: number): Observable<any> {
    return this.http.delete(this.url + '/' + id);
  }

}
