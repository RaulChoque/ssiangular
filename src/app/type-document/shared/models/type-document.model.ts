export class TypeDocument {
  id: number;
  descripcion: string;
  observaciones: string;

  constructor(id?: number, descripcion?: string, observaciones?: string) {
    this.id = id || 0;
    this.descripcion = descripcion || null;
    this.observaciones = observaciones || null;
  }
}
