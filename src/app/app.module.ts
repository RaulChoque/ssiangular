import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CustomMaterialModule} from './custom-material/custom-material.module';
import {AppRoutingModule} from './app-routing.module';
import {StatesModule} from './states/states.module';
import {TypeDocumentModule} from './type-document/type-document.module';
import {ProviderModule} from './provider/provider.module';
import {MachineryModule} from './machinery/machinery.module';
import {WorkModule} from './work/work.module';
import {AdvanceModule} from './advance/advance.module';


@NgModule({
  imports: [
    FormsModule,
    HttpClientModule,
    RouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    CustomMaterialModule,
    AppRoutingModule,
    StatesModule,
    TypeDocumentModule,
    ProviderModule,
    MachineryModule,
    WorkModule,
    AdvanceModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
